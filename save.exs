source = "C:\\Users\\Tang\\AppData\\Roaming\\7DaysToDie\\Saves\\Navezgane\\Tony2"
dest = "C:\\Users\\Tang\\7DTDBackups\\"

IO.puts("Backing up game files from #{source} ...")

now =
  DateTime.utc_now()
  |> DateTime.truncate(:second)
  |> DateTime.to_iso8601()
  |> String.replace(":", ".")
  |> String.replace("Z", "")

target_dir = "#{dest}#{now}"

IO.puts("To ... : #{target_dir}")

File.cp_r(source, target_dir)

IO.puts("Done !")
